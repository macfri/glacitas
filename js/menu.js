$(function(){

	var classNames = {
		0: 'web_trFilaRegistros1',
		1: 'web_trFilaRegistros2'
	};

	$('table#id_tabla_listado tr').not('.web_trCabecera').each(function(index) {
				
			this.className=classNames[index % 2];
			
			$(this).hover(function(){
				$(this).addClass('web_trFilaRegistros4');   
			},function(){      
				$(this).removeClass('web_trFilaRegistros4');  
			});
	
	});

	function verificarCheckbox(){

		var cantidad=$('#id_tabla_listado input[type=checkbox]:checked').not('#id_check_todos').length;
		
		if(cantidad>0) {
			$('.id_eliminar').css({opacity:1}).removeClass('web_boton1_on');
		}	else {
			$('.id_eliminar').css({opacity:0.3}).addClass('web_boton1_on');
		}
		
	}
	
	$('#id_tabla_listado input[type=checkbox]').attr('checked',false);
	
	$('#id_tabla_listado input[type=checkbox]').bind('click',function(){
		verificarCheckbox();
	})
	
	$('.id_eliminar').css({opacity:0.3}).addClass('web_boton1_on');

	
	$('#id_tabla_listado input[id^=check_]').bind('click',function(){
		
		if($(this).is(':checked')) {
				$(this).parent().parent().addClass('web_trFilaRegistros3');
		}	else {
				$('#id_check_todos').attr('checked',false);
				$(this).parent().parent().removeClass('web_trFilaRegistros3');
		}
		
	})	  
	
	$('#id_check_todos').bind('click',function(){
			
		var $dato=$( "#id_tabla_listado input[type=checkbox]").not($(this));

		if($(this).is(':checked')){
			$dato.attr('checked',$('#id_check_todos').is(':checked'));
			$dato.parent().parent().addClass('web_trFilaRegistros3');
		}else{
			$dato.attr('checked',$('#id_check_todos').is(':checked'));
			$dato.parent().parent().removeClass('web_trFilaRegistros3');
		}

		verificarCheckbox();

	})


	$('.id_agregar').bind('click',function(e){
		
		e.preventDefault();
		$('#frm').attr('action','agregar.php');
		$('#frm').submit();
		
	})
	
	$('.id_eliminar').bind('click',function(e){
	
		var c = e.target.className.split(" ");
		e.preventDefault();
		if(c[2]==undefined){
			e.preventDefault();
		//elementoSeleccionado();
			//alert('si');
		}
		
	})
			
			
})