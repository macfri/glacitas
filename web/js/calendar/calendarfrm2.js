// JavaScript Document
jQuery(function($){
				$.datepicker.setDefaults({showOn: 'both', buttonImageOnly: true,
	buttonImage: '../images/sistema/calendario.jpg', buttonText: 'Calendario',minDate: new Date()});
				$(".date-pick").datepicker({ dateFormat: 'dd-mm-yy',beforeShow: customRange});
	});
	
	function customRange(input) { 
    return {minDate: (input.id == 'txt_dsede' ? $('#txt_desde').datepicker('getDate') : null), 
        maxDate: (input.id == 'txt_desde' ? $('#txt_hasta').datepicker('getDate') : null)}; 
	} 