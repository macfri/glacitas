// JavaScript Document
jQuery(function($){
				$.datepicker.setDefaults({showOn: 'both', buttonImageOnly: true,
	buttonImage: '../images/sistema/calendario.jpg', buttonText: 'Calendario'});
				$(".date-pick").datepicker({ dateFormat: 'dd-mm-yy',beforeShow: customRange});
	});
	
	function customRange(input) { 
    return {minDate: (input.id == 'txt_hasta' ? $('#txt_desde').datepicker('getDate') : null), 
        maxDate: (input.id == 'txt_hasta' ? $('#txt_hasta').datepicker('getDate') : null)}; 
	} 
