$(document).ready(function(){

	jQuery.validator.addMethod("IsValidDate", 
							   function(value) 
							   {
									var arrDate = value.split("/");
									//console.debug(arrDate, "dia: ", arrDate[0], "mes: ", arrDate[1], "anio: ", arrDate[2]);
								   
									var arrMes30= new Array('2','4','6','9','11');

									for( var x=0; x<arrMes30.length; x++ )
									{
										if( parseInt(arrDate[1]) == arrMes30[x] )
										{
											if( arrDate[0] == 31 )
											{
												//console.debug("0");
												return false;
											}
										}
									}

									if( arrDate[1] == 2 )
									{
										if( arrDate[0] == 29 )
										{
											var rpta = ( arrDate[2] - 1980 ) % 4;
											if( rpta != 0 )
											{
												//console.debug("0");
												return false;
											}
											else if( arrDate[0] > 29 )
											{
												//console.debug("0");
												return false;
											}
										}
										if(arrDate[0]==30)
										{
											return false;
										}
									}
									//console.debug("1");
									return true;
								});


// validate signup form on keyup and submit
	$("#frmIns").validate({
		rules: {
			//TxtDoc: {
				//required: true,
				//digits: true,
				//minlength: 8,
				//maxlength: 8

			//},					
			TxtDoc: { required: true,
					  digits: true,
					  maxlength: 8,
					  minlength:8
				    },
			fecha_victim: { required: true,
							IsValidDate: true
							},
														
			TxtNombre: { required: true,
						 lettersonlylatam: true,
						 minlength:2
						},
			TxtApellidoPat: "required",
			TxtApellidoMat: "required",
			Sexo: "required",
			//EstadoC: "required",
			TxtDireccion: "required",
			cboDepartamento:{ required: function(element) {
//															console.debug($("#archivoupload:blank").size());
															if($("#cboPais").val() != "PE") { return false; }
															else { return true; }
														}
			
			                },
			cboProvincia:{ required: function(element) {
//															console.debug($("#archivoupload:blank").size());
															if($("#cboPais").val() != "PE") { return false; }
															else { return true; }
														}
			
			                },
			cboDistrito:{ required: function(element) {
//															console.debug($("#archivoupload:blank").size());
															if($("#cboPais").val() != "PE") { return false; }
															else { return true; }
														}
			
			                },
			TxtTelefonoCasa: {
				required: true,
				digits: true
			},
			TxtTelefonoAdic: {
				//required: true,
				digits: true
			},
			TxtCelular: {
				required: true,
				digits: true
			},
			cboCelular: "required",
			TxtMail: {
				required: true,
				email: true
			},
			//cboOcupacion: "required",
			//TxtOcupacion: {required: function(element) {
//															console.debug($("#archivoupload:blank").size());
															//if($("#cboOcupacion").val() == "OTR") { return true; }
															//else { return false; }
														//}
			
			//},
			//TxtMusica: "required",
			TeApuntas: "required",
			//TxtHistorica: "required",
			Txt_terminos: {
				required: true
			}
		},
		messages: {
			fecha_victim: "",
			TxtDia: "*",
			TxtMes: "*",
			TxtAnio: "*",
			TxtDoc: "*",
			TxtNombre: "*",
			TxtApellidoPat: "*",
			TxtApellidoMat: "*",
			//stadoC: "",
			Sexo: "",
			TxtDireccion: "*",
			cboDepartamento: "*",
			cboProvincia: "*",
			cboDistrito: "*",
			TxtTelefonoCasa: "*",
			TxtTelefonoAdic: "*",
			TxtCelular: "*",
			cboCelular: "*",
			TxtMail: "*",
			//cboOcupacion: "*",
			//TxtOcupacion: "*",
			//TxtMusica: "*",
			TeApuntas: "*",
			//TxtHistorica: "",
			Txt_terminos: ""
		}
	});
							
});

function valRegistro()
{
//	console.debug($("input[name='Txt_terminos']:checked").length);
	if($("input[name='Txt_terminos']:checked").length == 0) {
		alert("Debe estar de acuerdo con los terminos y condiciones del Registro");
	}
	$("#frmIns").submit();
}


jQuery.fn.reset = function () {
  $(this).each (function() { this.reset(); });
}

function limpiar()
{
	$("#frmIns").reset();
}

function validarFecha(dia, mes, anio){
	 var arrMes30= new Array('2','4','6','9','11');
			
			for( var x=0; x<arrMes30.length; x++ ){
				if( mes == arrMes30[x] ){
					if( dia == 31 ){
						return false;
					}
				}
			}
			
			if( mes == 2 ){
				if( dia == 29 ){
					var rpta = ( anio - 1980 ) % 4;
					if( rpta != 0 ){
						return false;
					}else if( dia > 29 ){
						return false;
					}
				}
			}
			
			return true;
		}
		

function updateBirthday()
{
	$("#fecha_victim").val($("#TxtDia option:selected").val() + "/" + $("#TxtMes option:selected").val() + "/" + $("#TxtAnio option:selected").val());
}

function replaceAcc (s)
{
 var r=s.toLowerCase();
 r = r.replace(/\u00E1/gi, "a");
 r = r.replace(/\u00E9/gi, "e");
 r = r.replace(/\u00ED/gi, "i");
 r = r.replace(/\u00F3/gi, "o");
 r = r.replace(/\u00FA/gi, "u");
 r = r.replace(/\u00F1/gi, "n");
 return r;
};

jQuery.validator.addMethod("lettersonlylatam", 
          function(value) 
          {
           var strName = replaceAcc(value);
           var regExp = /^([\s-a-zA-Z ])+$/;
           if(!strName.match(regExp))
           {
          return false;
           }
           else
           {
          return true;
           }
        });	