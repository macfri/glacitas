<?php

class Login extends Controller {
    function Login(){
        parent::Controller();	
        $this->load->model('tbl_usuario_model','obj_usuario_hijo');                
		$this->load->library('Session');
        $this->load->library('email');
        $this->load->helper('security');
        $this->load->helper('funciones');
        $this->load->helper('text');
    }

    function index(){        
        $this->load->view('usuario_acceso_view.php');
    }

    function proc_login(){        
        $txt_email = $this->input->post('txt_email');
        $txt_password  = trim($this->input->post('txt_password'));
        $txt_password  = dohash($txt_password, 'md5');        
	$email = $this->obj_usuario_hijo->verificar_email($txt_email);
        if ($email){                        
            $pass = $this->obj_usuario_hijo->verificar_password($txt_password);            
            if($pass){
                $activo = $this->obj_usuario_hijo->verificar_cuenta($txt_email,$txt_password);                
                if($activo){
                    echo "cuenta";
                }else{
                    echo 2;
                }
            }else{
                echo 1;
            }
        }else{
            echo 0;
        }
    }

    public function cuenta(){
        echo "hola como estas";
    }

    public function registro(){        
        
        $data["usuario"][0] = $this->obj_usuario->obj_campos;
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_usuario = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_usuario != ""){
            $this->obj_usuario->obj_condiciones->agregar_condicion("pk_usuario='".$pk_usuario."'");
            $data["usuario"] = $this->obj_usuario->search();
        }  
        $this->load->view('usuario_registro_view.php',$data);        
    }

    function validate(){        
        $pk_usuario = $this->input->post("pk_usuario");

        $this->form_validation->set_rules('txt_nombre','Nombre','required|trim');
	$this->form_validation->set_rules('txt_apellidos','Apellidos','required|trim');
	$this->form_validation->set_rules('txt_email','email','required|valid_email|trim');
	$this->form_validation->set_rules('date_fecha_nacimiento','Fecha de nacimiento','required|trim');
	$this->form_validation->set_rules('txt_nickname','Usuario','required|trim|min_length[5]');
        $this->form_validation->set_rules('txt_nickname','Usuario','callback_validate_usuario[txt_nickname]');
        $this->form_validation->set_rules('txt_nickname','Usuario','callback_moderacion[txt_nickname]');
	$this->form_validation->set_rules('txt_password','Contraseña','required|trim|md5');
	$this->form_validation->set_rules('txt_re_password','Confirmar contraseña','required|matches[txt_password]|trim|md5');
	$this->form_validation->set_rules('txt_apoderado','Apoderado','required|trim');
	$this->form_validation->set_rules('txt_email_apoderado','email apoderado','required|valid_email|trim');
        $this->form_validation->set_rules('txt_email','email','callback_validate_mail[txt_email]');

        $this->form_validation->set_message('required','Debe introducir el campo %s');
        $this->form_validation->set_message('min_length','El campo %s debe ser almenos %5 carácteres');
        $this->form_validation->set_message('valid_email','Debe escribir una dirección de eamil no es correcta en %s');
        $this->form_validation->set_message('matches','Las contraseñas %s y %s no conciden');

        if ($this->form_validation->run()== FALSE){
            $data["usuario"][0] = $this->obj_usuario->obj_campos;            
            $this->load->view('usuario_registro_view.php',$data);
        }else{
            $date = inc_invertir_fecha($this->input->post('date_fecha_nacimiento'),1);

            $data = array(
                'txt_nombre' => $this->input->post('txt_nombre'),
                'txt_apellidos' => $this->input->post('txt_apellidos'),
                'txt_email' => $this->input->post('txt_email'),
                'date_fecha_nacimiento' => $date,
                'txt_nickname' => $this->input->post('txt_nickname'),
                'txt_password' => $this->input->post('txt_password'),
                'txt_re_password' => $this->input->post('txt_re_password'),
                'txt_apoderado' => $this->input->post('txt_apoderado'),
                'txt_email_apoderado' => $this->input->post('txt_email_apoderado'),
                'int_activo' => '0'
            );
            if ($pk_usuario != ""){
                $this->obj_usuario->update($pk_usuario, $data);
            }else{
                $this->obj_usuario->insert($data);

                /*RECUPERAMOS*/
                $mail_h = $this->input->post('txt_email');
                $mail_p = $this->input->post('txt_email_apoderado');
                $usuario = $this->input->post('txt_nickname');                
                $dato["usuario"]   = $this->obj_usuario_hijo->recuperar_usuario($mail_h,$mail_p,$usuario);                

                $this->email->from('marco_cd80@hotmail.com', 'Tu nombre');
                $this->email->to('mcristobal@tribalperu.com');
                $this->email->subject('Active la cuenta de su hijo en Glacitas');
                $this->email->message($html);
                $this->email->send();

                $config['protocol'] = 'mail';
                $config['wordwrap'] = FALSE;
                $config['mailtype'] = 'html';
//                $config['protocol'] = 'sendmail';
//                $config['mailpath'] = '/usr/sbin/sendmail';
//                $config['charset'] = 'iso-8859-1';
//                $config['wordwrap'] = TRUE;
                $this->email->initialize($config);

                $html = $this->load->view('mail/cuenta_usuario', $dato, TRUE);
            }            
        }        
    }

    public function activar($pk_usuario = null){
        $dato = array(
           'int_activo' => '1'
        );
        $this->obj_usuario_hijo->activar_usuario($pk_usuario,$dato);        
        $data["usuario"] = $this->obj_usuario_hijo->datos_usuario($pk_usuario);
        $this->load->view('usuario_activar_view.php',$data);
    }

    public function confirmar(){
        $data['url'] = HOME_URL;
        $this->load->view('usuario_confirmar_view.php',$data);        
    }

    public function logout(){       
	$this->session->unset_userdata('nombre');
	$this->session->destroy();
        redirect('admin/login');
    }

    public function validate_mail($txt_email) // normal callback function
     {
        if ($this->obj_usuario_hijo->existe_mail(trim($txt_email))) {
           return TRUE;
        }
        $this->form_validation->set_message('validate_mail', 'El mail ya existe!');
        return FALSE;
     }

    public function validate_usuario($usuario) // normal callback function
     {
        if ($this->obj_usuario_hijo->existe_usuario(trim($usuario))) {
           return TRUE;
        }
        $this->form_validation->set_message('validate_usuario', 'El usuario ya existe!');
        return FALSE;
     }

    public function moderacion($usuario){
        $disallowed = array('mierda', '666', 'carajo', 'basura');
        $string = word_censor($usuario, $disallowed, 'true');

        if ($string == "true"){
            $this->form_validation->set_message('moderacion', 'El nombre del usuario ingresado no esta permitido!');
            return FALSE;
        }
        return true;

     }
}
?>
