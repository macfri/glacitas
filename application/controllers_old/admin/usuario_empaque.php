<?php
/*****
* Generator Controlers MC v.1.0
* DATE: 13/09/2010
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
******/
class Usuario_empaque extends Controller {
    function usuario_empaque(){
        parent::Controller();
        $this->load->model("tbl_usuario_empaque_model","obj_usuario_empaque_hijo");

        $this->template->set_template("default");
        $this->template->add_js("js/DD_belatedPNG_0.0.8a-min.js");
        $this->template->add_js("js/jquery.js");
        $this->template->add_js("js/menu.js");
        $this->template->add_js("js/mas_datos.js");
        $this->template->add_css("styles/maq.css");
        $this->template->add_css("styles/web.css");
        $this->template->write_view("region_usuario", "admin/home/layout_body_usuario", array(), TRUE);
        $this->template->write_view("region_modulo", "admin/home/layout_body_modulo", array(), TRUE);
        $this->load->library("session");
    }

    function index(){
        $txt_buscar = $this->input->post("txt_buscar");

        /* CREACION DE LA CONSULTA SQL*/
        $this->obj_usuario_empaque->obj_campos_mostrar->seleccionar();
	$this->obj_usuario_empaque->obj_orden->agregar_orden();
	$this->obj_usuario_empaque->obj_condiciones->agregar_condicion();

        /* CONFIGURANDO LA PAGINACION*/
        $config["base_url"] = trim(site_url(), "/") . "/admin/usuario_empaque/index/";
        $config["total_rows"] = $this->obj_usuario_empaque->total_records();
        $config["per_page"] = "10";
        $config["uri_segment"] = 4;
        $config["num_links"] = 3;        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
	$data["usuario_empaque"] = $this->obj_usuario_empaque->search_data($config["per_page"],$this->uri->segment($config["uri_segment"], 0));

        /*LLAMANDO A LA VISTA*/

        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/usuario_empaque_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }

        
    }

    function load(){

        $data["usuario_empaque"][0] = $this->obj_usuario_empaque->obj_campos;        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_usuario_empaque = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_usuario_empaque != ""){
            $this->obj_usuario_empaque->obj_condiciones->agregar_condicion("pk_usuario_empaque=");
            $data["usuario_empaque"] = $this->obj_usuario_empaque->search();
        }        
        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/usuario_empaque_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }
    }

    function validate(){
        $pk_usuario_empaque = $this->input->post("pk_usuario_empaque");

        $this->form_validation->set_rules('fk_empaque','','required|trim');
	$this->form_validation->set_rules('fk_usuario','','required|trim');
	$this->form_validation->set_rules('txt_cantidad','','required|trim');
	$this->form_validation->set_rules('date_fecha','','required|trim');
	
$this->form_validation->set_message('required','Debe introducir el campo %s');
            
        if ($this->form_validation->run()== FALSE){
            $data["usuario_empaque"][0] = $this->obj_usuario_empaque->obj_campos;
            $this->template->write_view("region_contenido", "admin/usuario_empaque_form", $data, TRUE);
            $this->template->render();
        }else{
            $data = array(
                'fk_empaque' => $this->input->post('fk_empaque'),
	'fk_usuario' => $this->input->post('fk_usuario'),
	'txt_cantidad' => $this->input->post('txt_cantidad'),
	'date_fecha' => $this->input->post('date_fecha')
            );
            if ($pk_usuario_empaque != ""){
                $this->obj_usuario_empaque->update($pk_usuario_empaque, $data);
            }else{
                $this->obj_usuario_empaque->insert($data);                
            }
            $_POST=NULL;
            redirect("admin/home");
        }
    }

    function delete(){        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_usuario_empaque = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_usuario_empaque != ""){
            $this->obj_usuario_empaque->delete($pk_usuario_empaque);
        }
        /*LLAMANDO A LA VISTA*/
        $this->index();
    }

    function delete_seleccionado(){
        $check =  $this->input->post("grupo_check");
        if (count($check)>= 0 and $check[0] != "" ){
            $this->obj_usuario_empaque->delete_seleccionado($check);
            redirect("admin/home");
        }else{
            redirect("admin/home");
        }
    }


}
?>