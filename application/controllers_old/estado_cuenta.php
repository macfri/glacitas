<?php
/*****
* Generator Controlers MC v.1.0
* DATE: 13/09/2010
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
******/
class Estado_cuenta extends Controller {
    function estado_cuenta(){
        parent::Controller();
        $this->load->model("tbl_estado_cuenta_model","obj_estado_cuenta_hijo");

        $this->template->set_template("default");
        $this->template->add_js("js/DD_belatedPNG_0.0.8a-min.js");
        $this->template->add_js("js/jquery.js");
        $this->template->add_js("js/menu.js");
        $this->template->add_js("js/mas_datos.js");
        $this->template->add_css("styles/maq.css");
        $this->template->add_css("styles/web.css");
        $this->template->write_view("region_usuario", "admin/home/layout_body_usuario", array(), TRUE);
        $this->template->write_view("region_modulo", "admin/home/layout_body_modulo", array(), TRUE);
        $this->load->library("session");
    }

    function index(){
       echo "hola como estas";

        
    }

    function load(){

        $data["estado_cuenta"][0] = $this->obj_estado_cuenta->obj_campos;        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_estado_cuenta = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_estado_cuenta != ""){
            $this->obj_estado_cuenta->obj_condiciones->agregar_condicion("pk_estado_cuenta=");
            $data["estado_cuenta"] = $this->obj_estado_cuenta->search();
        }        
        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/estado_cuenta_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }
    }

    function validate(){
        $pk_estado_cuenta = $this->input->post("pk_estado_cuenta");

        $this->form_validation->set_rules('fk_usuario','','required|trim');
	$this->form_validation->set_rules('date_registro_transacciones','','required|trim');
	$this->form_validation->set_rules('int_cantidad_monedas','','required|trim');
	$this->form_validation->set_rules('int_concepto','','required|trim');
	$this->form_validation->set_rules('id_registro_concepto','','required|trim');
	
$this->form_validation->set_message('required','Debe introducir el campo %s');
            
        if ($this->form_validation->run()== FALSE){
            $data["estado_cuenta"][0] = $this->obj_estado_cuenta->obj_campos;
            $this->template->write_view("region_contenido", "admin/estado_cuenta_form", $data, TRUE);
            $this->template->render();
        }else{
            $data = array(
                'fk_usuario' => $this->input->post('fk_usuario'),
	'date_registro_transacciones' => $this->input->post('date_registro_transacciones'),
	'int_cantidad_monedas' => $this->input->post('int_cantidad_monedas'),
	'int_concepto' => $this->input->post('int_concepto'),
	'id_registro_concepto' => $this->input->post('id_registro_concepto')
            );
            if ($pk_estado_cuenta != ""){
                $this->obj_estado_cuenta->update($pk_estado_cuenta, $data);
            }else{
                $this->obj_estado_cuenta->insert($data);                
            }
            $_POST=NULL;
            redirect("admin/home");
        }
    }

    function delete(){        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_estado_cuenta = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_estado_cuenta != ""){
            $this->obj_estado_cuenta->delete($pk_estado_cuenta);
        }
        /*LLAMANDO A LA VISTA*/
        $this->index();
    }

    function delete_seleccionado(){
        $check =  $this->input->post("grupo_check");
        if (count($check)>= 0 and $check[0] != "" ){
            $this->obj_estado_cuenta->delete_seleccionado($check);
            redirect("admin/home");
        }else{
            redirect("admin/home");
        }
    }


}
?>