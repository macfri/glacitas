<?php
/*****
* Generator Controlers MC v.1.0
* DATE: 13/09/2010
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
******/
class Invitaciones extends Controller {
    function invitaciones(){
        parent::Controller();
        $this->load->model("tbl_invitaciones_model","obj_invitaciones_hijo");

        $this->template->set_template("default");
        $this->template->add_js("js/DD_belatedPNG_0.0.8a-min.js");
        $this->template->add_js("js/jquery.js");
        $this->template->add_js("js/menu.js");
        $this->template->add_js("js/mas_datos.js");
        $this->template->add_css("styles/maq.css");
        $this->template->add_css("styles/web.css");
        $this->template->write_view("region_usuario", "admin/home/layout_body_usuario", array(), TRUE);
        $this->template->write_view("region_modulo", "admin/home/layout_body_modulo", array(), TRUE);
        $this->load->library("session");
    }

    function index(){
        $txt_buscar = $this->input->post("txt_buscar");

        /* CREACION DE LA CONSULTA SQL*/
        $this->obj_invitaciones->obj_campos_mostrar->seleccionar();
	$this->obj_invitaciones->obj_orden->agregar_orden();
	$this->obj_invitaciones->obj_condiciones->agregar_condicion();

        /* CONFIGURANDO LA PAGINACION*/
        $config["base_url"] = trim(site_url(), "/") . "/admin/invitaciones/index/";
        $config["total_rows"] = $this->obj_invitaciones->total_records();
        $config["per_page"] = "10";
        $config["uri_segment"] = 4;
        $config["num_links"] = 3;        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
	$data["invitaciones"] = $this->obj_invitaciones->search_data($config["per_page"],$this->uri->segment($config["uri_segment"], 0));

        /*LLAMANDO A LA VISTA*/

        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/invitaciones_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }

        
    }

    function load(){

        $data["invitaciones"][0] = $this->obj_invitaciones->obj_campos;        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_invitaciones = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_invitaciones != ""){
            $this->obj_invitaciones->obj_condiciones->agregar_condicion("pk_invitaciones=");
            $data["invitaciones"] = $this->obj_invitaciones->search();
        }        
        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/invitaciones_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }
    }

    function validate(){
        $pk_invitaciones = $this->input->post("pk_invitaciones");

        $this->form_validation->set_rules('fk_juegos','','required|trim');
	$this->form_validation->set_rules('fk_usuario','','required|trim');
	$this->form_validation->set_rules('date_fecha_invitacion','','required|trim');
	$this->form_validation->set_rules('txt_nombre','','required|trim');
	$this->form_validation->set_rules('txt_asunto','','required|trim');
	$this->form_validation->set_rules('txt_mensaje','','required|trim');
	$this->form_validation->set_rules('int_estado','','required|trim');
	$this->form_validation->set_rules('fk_invitado','','required|trim');
	$this->form_validation->set_rules('date_fecha_caduca','','required|trim');
	
$this->form_validation->set_message('required','Debe introducir el campo %s');
            
        if ($this->form_validation->run()== FALSE){
            $data["invitaciones"][0] = $this->obj_invitaciones->obj_campos;
            $this->template->write_view("region_contenido", "admin/invitaciones_form", $data, TRUE);
            $this->template->render();
        }else{
            $data = array(
                'fk_juegos' => $this->input->post('fk_juegos'),
	'fk_usuario' => $this->input->post('fk_usuario'),
	'date_fecha_invitacion' => $this->input->post('date_fecha_invitacion'),
	'txt_nombre' => $this->input->post('txt_nombre'),
	'txt_asunto' => $this->input->post('txt_asunto'),
	'txt_mensaje' => $this->input->post('txt_mensaje'),
	'int_estado' => $this->input->post('int_estado'),
	'fk_invitado' => $this->input->post('fk_invitado'),
	'date_fecha_caduca' => $this->input->post('date_fecha_caduca')
            );
            if ($pk_invitaciones != ""){
                $this->obj_invitaciones->update($pk_invitaciones, $data);
            }else{
                $this->obj_invitaciones->insert($data);                
            }
            $_POST=NULL;
            redirect("admin/home");
        }
    }

    function delete(){        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_invitaciones = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_invitaciones != ""){
            $this->obj_invitaciones->delete($pk_invitaciones);
        }
        /*LLAMANDO A LA VISTA*/
        $this->index();
    }

    function delete_seleccionado(){
        $check =  $this->input->post("grupo_check");
        if (count($check)>= 0 and $check[0] != "" ){
            $this->obj_invitaciones->delete_seleccionado($check);
            redirect("admin/home");
        }else{
            redirect("admin/home");
        }
    }


}
?>