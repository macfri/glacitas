<?php
class Login extends Controller {
     function Login()
     {
      parent::Controller();
      $this->load->model('users');
      $this->load->library('session');
     }
     function _foo($str = '', $val = '') // normal callback function
     {
          if ($str != $val) {
           $this->form_validation->set_message('_foo', 'The %s field must be ' . $val);
           return FALSE;
          }
          return TRUE;
     }
     function _valid_login($str = '', $params = '') // callback function with multiple parameters
     {
          list($username, $password) = explode(',', $params);
          if ($this->users->valid_login($username, $password)) {
           return TRUE;
          }
          $this->form_validation->set_message('_validate_login', 'No matching accounts!');
          return FALSE;
     }
     function index()
     {
          $username = $this->input->post('username', TRUE);
          $password = $this->input->post('password', TRUE);
          $params = "{$username},{$password}";
          $this->load->library('form_validation');
          $this->form_validation->set_rules('username', 'Username', "xss_clean|callback__foo[bar]|callback__valid_login[{$params}]");
          $this->form_validation->set_rules('password', 'Password', 'trim|required|min_length[6]|xss_clean');
          if ($this->form_validation->run() == FALSE) {
               // invalid or initial request
               $this->load->view('login');
              } else {
               // success
               redirect('account');
          }
     }
}

class Users extends Model {
 function Users(){
  parent::Model();
 }
 function valid_login($username = '', $password = '')
 {
  $this->db->where('username', $username);
  $this->db->where('password', $password);
  return $this->db->count_all_results($this->table);
 }
}