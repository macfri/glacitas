<?php
class front_end extends Controller 
{
	public function front_end()
    {
		parent::Controller();
		$this->load->model('user_model');
		
				$this->output->enable_profiler(TRUE);
    }
    
    public function index()
    {
        $this->load->view('web/index.html', array());
    }
    
    public function user_view()
    {
        $this->load->view('web/user_register.html', array());
    }
        
    public function user_save()
    {
    	$data = array(
    	'txt_nombre' => $this->input->post('txt_nombre'),
    	'txt_apellidos' => $this->input->post('txt_apellidos'),
    	'txt_email' =>$this->input->post('txt_email'),
    	'txt_email_apoderado' => $this->input->post('txt_email_apoderado'),
    	'txt_password' => $this->input->post('txt_password')
    	);

    	if (!$this->user_model->check_email($this->input->post('txt_email')))
    	{
			if ($this->user_model->save($data)){
				$message = "Muchas Gracias por regisrart, para poder logearte, 
				debes confirmar el registro por tu correo electronico!";
				
				$id = $this->db->insert_id();
		    	
				if (!session_id()){session_start();}
				$data_user_session['id']=$id;
				$data_user_session['name']=$data['txt_nombre'];
				$data_user_session['email']=$data['txt_email'];
				$data_user_session['logged']=true;
				$_SESSION['user_data'] = $data_user_session;
						
			}else {
				$message = "Error al grabar";
			}
    	}
    	else 
    	{
    		$message = 'Email existe';
    	}
		
		$data= array('message'=>$message);
 		$this->load->view('web/user_thanks.html', $data);
    }
    
    public function user_login()
    {
        $this->load->view('web/user_login.html', array());
    }

    public function user_login_proc()
    {
    	
    	// SI EL USUARIO EXISTE UPDATE TABLA INVITADO A 1
    	
    	$txt_email = $this->input->post('txt_email');
        $txt_password = $this->input->post('txt_password');
        
		if ($this->user_model->doCheckExist($txt_email, $txt_password)){
			$data = $this->user_model->doAll($txt_email, $txt_password);
			if (is_object($data)){
				if (!session_id()){session_start();}
				
				$pk_usuario = $data->pk_usuario;         
				
				$data_user_session['id']=$data->pk_usuario;;
				$data_user_session['name']=$data->txt_nombre;
				$data_user_session['email']=$data->txt_email;
				$data_user_session['logged']=true;
				$_SESSION['user_data'] = $data_user_session;
				$message = 'Login correcto';
			}else{
				$message = 'Login Incorrecto';
			}
		}else {
			$message = 'NO TOTAL';
		}

		$data= array('message'=>$message);
 		$this->load->view('web/user_login_proc.html', $data);
    }

    public function user_invite()
    {
    	$data = array('message'=>'Deslogueado..');
        $this->load->view('web/user_invite.html', $data);
    }  
    
    public function user_invite_proc()
    {
		$pk_usuario 	= $this->__getSessionId();
		$pk_juego 		= $this->__getSessionIdJuego();
		$pk_invitado 	= $this->user_model->getPkByEmail($this->input->post('txt_email'));

		if ($pk_usuario<1){die('Debe loguearse');}
		if ($pk_juego<1){die('invalido id juego');}
		if ($pk_invitado<1){die('invalido id usuario');}
					
		$data = array(
			'fk_juego'=>$pk_juego,
			'fk_usuario'=>$pk_usuario,
			'fk_invitado'=>$pk_invitado,
			'int_estado'=>0,
			'int_veces'=>0,
			'date_registro'=>date("Y-m-d H:i:s")
			);

		if ($this->user_model->saveInvitado($data)){
			$message = "Grabado con exito!";
		}else {
			$message = "Error al grabar";
		}
		
    	$data = array('message'=>$message);
        $this->load->view('web/user_invite_proc.html', $data);
    }  

    public function user_logout()
    {
   		unset($_SESSION);
    	$data = array('message'=>'Deslogueado..');
        $this->load->view('web/user_logout.html', $data);
    }    
    
    public function user_play()
    {
        $this->load->view('web/user_play_start.html', array());
    }    
    
    public function user_play_start($id_juego)
    {
		$id_user = $this->__getSessionId();
		if ($id_user<1){die('Debe loguearse');}
		
		$id_juego = (int)$id_juego;
		if ($id_juego<1){die('Codigo jugada invalido');}	    	
		
		$date_fe = date("Y-m-d H:i:s");			

		$data = array(
    	'fk_juego' => $id_juego,
    	'fk_usuario' => $id_user,
    	'fecha_registro' => $date_fe,
    	'int_estado' => 0
    	);
    			
		if ($this->user_model->saveUsuarioJuego($data)){
			$id_usuario_juego = $this->db->insert_id(); // detalle
	    	$data = array(
	    	'fk_accion' => 2,
	    	'fk_usuario' => $id_user,
	    	'date_registro' =>$date_fe,
	    	'id_usuario_accion_detalle' => $id_usuario_juego
	    	);
			if ($this->user_model->saveUsuarioAccion($data)){
				if (!session_id()){session_start();}
				$data_play['id_juego']=$id_juego;
				$data_play['id_usuario_juego']=$id_usuario_juego;
				$_SESSION['data_play'] = $data_play;
				$message='OK';
			}else {
				$message = 'NO OK';
			}
		}else {
			$message ='NO OK';
		}
		$data = array('message'=>$message, 'id_juego'=>$id_juego);
		$this->load->view('web/user_play_end.html', $data);
    }
        
    public function user_play_end()
    {
    	$id_usuario_juego = $this->__getSessionIdUsuarioJuego();
		if ($id_usuario_juego<1){die('ID USER juego INVALID');}
    	$int_estado = rand(1, 2);
		$date_fe = date("Y-m-d H:i:s");
		$data = array('int_estado'=>$int_estado, 'fecha_mod'=>$date_fe);
		if ($this->user_model->updateStateUsuarioJuego($id_usuario_juego, $data)){
			die('GANO');
		}else {
			die('PERDIO');			
		}
    }
    
    public function user_send_packing()
    {
        $this->load->view('web/user_packing.html', array());    	
    }

    public function user_send_packing_proc()
    {
    	$id_user = $this->__getSessionId();
    	$date_fe = date("Y-m-d H:i:s");
    	$txt_numero_codigo = $this->input->post('txt_numero_codigo');

    	$data = $this->user_model->getEmpaque($txt_numero_codigo);
    	
    	$_state = 1;
    	$_pk_empaque = 0;
    	if (is_object($data)) {
				$_state = (int)$data->int_estado;
				$_pk_empaque = (int)$data->pk_empaque;    	
    	}
    	
		if ($_pk_empaque>0 && $id_user>0){
			if ($_state==0){
				$sql = "update tbl_empaque set int_estado=1 
				where txt_numero_codigo='$txt_numero_codigo'";
	    		if ($this->db->query($sql)){
					$id_usuario_juego = $this->db->insert_id();
			    	$data = array(
			    	'fk_accion' => 1,
			    	'fk_usuario' => $id_user,
			    	'date_registro' =>$date_fe,
			    	'id_usuario_accion_detalle' => $_pk_empaque
			    	);
					$this->user_model->saveUsuarioAccion($data);
	    		}
			}
		}
        $this->load->view('web/user_packing.html', array());    	
    }    
    
    public function getSession()
    {
		if (!session_id()){session_start();}
		print_r($_SESSION);
    }
    
    private function __getSessionId()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['user_data']['id'])){
			$session_id = (int)$_SESSION['user_data']['id'];
		}
		return $session_id;
    }
    
    private function __getSessionIdJuego()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['id_juego'])){
			$session_id = (int)$_SESSION['data_play']['id_juego'];
		}
		return $session_id;
    }
    
    private function __getSessionIdUsuarioJuego()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['id_usuario_juego'])){
			$session_id = (int)$_SESSION['data_play']['id_usuario_juego'];
		}
		return $session_id;
    }    
    
    public function user_invitaciones()
    {
    	$id_user = $this->__getSessionId();
    	$data = $this->user_model->getInvitados($id_user);
    	//print_r($data);
        $this->load->view('web/user_invitaciones_help.html', array('data'=>$data));    	
    }   
}
?>