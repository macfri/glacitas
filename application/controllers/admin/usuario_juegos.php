<?php
/*****
* Generator Controlers MC v.1.0
* DATE: 13/09/2010
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
******/
class Usuario_juegos extends Controller {
    function usuario_juegos(){
        parent::Controller();
        $this->load->model("tbl_usuario_juegos_model","obj_usuario_juegos_hijo");

        $this->template->set_template("default");
        $this->template->add_js("js/DD_belatedPNG_0.0.8a-min.js");
        $this->template->add_js("js/jquery.js");
        $this->template->add_js("js/menu.js");
        $this->template->add_js("js/mas_datos.js");
        $this->template->add_css("styles/maq.css");
        $this->template->add_css("styles/web.css");
        $this->template->write_view("region_usuario", "admin/home/layout_body_usuario", array(), TRUE);
        $this->template->write_view("region_modulo", "admin/home/layout_body_modulo", array(), TRUE);
        $this->load->library("session");
    }

    function index(){
        $txt_buscar = $this->input->post("txt_buscar");

        /* CREACION DE LA CONSULTA SQL*/
        $this->obj_usuario_juegos->obj_campos_mostrar->seleccionar();
	$this->obj_usuario_juegos->obj_orden->agregar_orden();
	$this->obj_usuario_juegos->obj_condiciones->agregar_condicion();

        /* CONFIGURANDO LA PAGINACION*/
        $config["base_url"] = trim(site_url(), "/") . "/admin/usuario_juegos/index/";
        $config["total_rows"] = $this->obj_usuario_juegos->total_records();
        $config["per_page"] = "10";
        $config["uri_segment"] = 4;
        $config["num_links"] = 3;        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
	$data["usuario_juegos"] = $this->obj_usuario_juegos->search_data($config["per_page"],$this->uri->segment($config["uri_segment"], 0));

        /*LLAMANDO A LA VISTA*/

        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/usuario_juegos_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }

        
    }

    function load(){

        $data["usuario_juegos"][0] = $this->obj_usuario_juegos->obj_campos;        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_usuario_juegos = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_usuario_juegos != ""){
            $this->obj_usuario_juegos->obj_condiciones->agregar_condicion("pk_usuario_juegos=");
            $data["usuario_juegos"] = $this->obj_usuario_juegos->search();
        }        
        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/usuario_juegos_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }
    }

    function validate(){
        $pk_usuario_juegos = $this->input->post("pk_usuario_juegos");

        $this->form_validation->set_rules('fk_juegos','','required|trim');
	$this->form_validation->set_rules('fk_usuario','','required|trim');
	$this->form_validation->set_rules('int_rol','','required|trim');
	$this->form_validation->set_rules('fecha_registro','','required|trim');
	$this->form_validation->set_rules('int_estado','','required|trim');
	
$this->form_validation->set_message('required','Debe introducir el campo %s');
            
        if ($this->form_validation->run()== FALSE){
            $data["usuario_juegos"][0] = $this->obj_usuario_juegos->obj_campos;
            $this->template->write_view("region_contenido", "admin/usuario_juegos_form", $data, TRUE);
            $this->template->render();
        }else{
            $data = array(
                'fk_juegos' => $this->input->post('fk_juegos'),
	'fk_usuario' => $this->input->post('fk_usuario'),
	'int_rol' => $this->input->post('int_rol'),
	'fecha_registro' => $this->input->post('fecha_registro'),
	'int_estado' => $this->input->post('int_estado')
            );
            if ($pk_usuario_juegos != ""){
                $this->obj_usuario_juegos->update($pk_usuario_juegos, $data);
            }else{
                $this->obj_usuario_juegos->insert($data);                
            }
            $_POST=NULL;
            redirect("admin/home");
        }
    }

    function delete(){        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_usuario_juegos = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_usuario_juegos != ""){
            $this->obj_usuario_juegos->delete($pk_usuario_juegos);
        }
        /*LLAMANDO A LA VISTA*/
        $this->index();
    }

    function delete_seleccionado(){
        $check =  $this->input->post("grupo_check");
        if (count($check)>= 0 and $check[0] != "" ){
            $this->obj_usuario_juegos->delete_seleccionado($check);
            redirect("admin/home");
        }else{
            redirect("admin/home");
        }
    }


}
?>