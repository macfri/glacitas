<?php
/*****
* Generator Controlers MC v.1.0
* DATE: 13/09/2010
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
******/
class Juego_invitado extends Controller {
    function juego_invitado(){
        parent::Controller();
        $this->load->model("tbl_juego_invitado_model","obj_juego_invitado_hijo");

        $this->template->set_template("default");
        $this->template->add_js("js/DD_belatedPNG_0.0.8a-min.js");
        $this->template->add_js("js/jquery.js");
        $this->template->add_js("js/menu.js");
        $this->template->add_js("js/mas_datos.js");
        $this->template->add_css("styles/maq.css");
        $this->template->add_css("styles/web.css");
        $this->template->write_view("region_usuario", "admin/home/layout_body_usuario", array(), TRUE);
        $this->template->write_view("region_modulo", "admin/home/layout_body_modulo", array(), TRUE);
        $this->load->library("session");
    }

    function index(){
        $txt_buscar = $this->input->post("txt_buscar");

        /* CREACION DE LA CONSULTA SQL*/
        $this->obj_juego_invitado->obj_campos_mostrar->seleccionar();
	$this->obj_juego_invitado->obj_orden->agregar_orden();
	$this->obj_juego_invitado->obj_condiciones->agregar_condicion();

        /* CONFIGURANDO LA PAGINACION*/
        $config["base_url"] = trim(site_url(), "/") . "/admin/juego_invitado/index/";
        $config["total_rows"] = $this->obj_juego_invitado->total_records();
        $config["per_page"] = "10";
        $config["uri_segment"] = 4;
        $config["num_links"] = 3;        
        $this->pagination->initialize($config);
        $data["pagination"] = $this->pagination->create_links();
	$data["juego_invitado"] = $this->obj_juego_invitado->search_data($config["per_page"],$this->uri->segment($config["uri_segment"], 0));

        /*LLAMANDO A LA VISTA*/

        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/juego_invitado_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }

        
    }

    function load(){

        $data["juego_invitado"][0] = $this->obj_juego_invitado->obj_campos;        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_juego_invitado = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_juego_invitado != ""){
            $this->obj_juego_invitado->obj_condiciones->agregar_condicion("pk_juego_invitado=");
            $data["juego_invitado"] = $this->obj_juego_invitado->search();
        }        
        if($this->session->userdata("nombre")) {
            $this->template->write_view("region_contenido", "admin/juego_invitado_list", $data, TRUE);
            $this->template->render();
        }else {
            redirect("admin/login");
        }
    }

    function validate(){
        $pk_juego_invitado = $this->input->post("pk_juego_invitado");

        $this->form_validation->set_rules('fk_usuario_juego','','required|trim');
	$this->form_validation->set_rules('fk_invitaciones','','required|trim');
	$this->form_validation->set_rules('estadoInvitado','','required|trim');
	$this->form_validation->set_rules('estadoParticipante','','required|trim');
	$this->form_validation->set_rules('estadoJuego','','required|trim');
	
$this->form_validation->set_message('required','Debe introducir el campo %s');
            
        if ($this->form_validation->run()== FALSE){
            $data["juego_invitado"][0] = $this->obj_juego_invitado->obj_campos;
            $this->template->write_view("region_contenido", "admin/juego_invitado_form", $data, TRUE);
            $this->template->render();
        }else{
            $data = array(
                'fk_usuario_juego' => $this->input->post('fk_usuario_juego'),
	'fk_invitaciones' => $this->input->post('fk_invitaciones'),
	'estadoInvitado' => $this->input->post('estadoInvitado'),
	'estadoParticipante' => $this->input->post('estadoParticipante'),
	'estadoJuego' => $this->input->post('estadoJuego')
            );
            if ($pk_juego_invitado != ""){
                $this->obj_juego_invitado->update($pk_juego_invitado, $data);
            }else{
                $this->obj_juego_invitado->insert($data);                
            }
            $_POST=NULL;
            redirect("admin/home");
        }
    }

    function delete(){        
        $arr_url = $this->uri->uri_to_assoc(2);
        $pk_juego_invitado = count($arr_url)==2?$arr_url["id"]:"";

        if ($pk_juego_invitado != ""){
            $this->obj_juego_invitado->delete($pk_juego_invitado);
        }
        /*LLAMANDO A LA VISTA*/
        $this->index();
    }

    function delete_seleccionado(){
        $check =  $this->input->post("grupo_check");
        if (count($check)>= 0 and $check[0] != "" ){
            $this->obj_juego_invitado->delete_seleccionado($check);
            redirect("admin/home");
        }else{
            redirect("admin/home");
        }
    }


}
?>