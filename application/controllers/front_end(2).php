<?php

//echo realpath('./');
//die();

require('./application/libraries/app_controller.php');

class front_end extends app_controller   
{
	public function front_end()
    {
		parent::app_controller();

    }
    
    public function index()
    {
		$this->template->render('web/index.html');
    }
    
    public function user_view()
    {
		$this->template->setLayout('layout_popup.html');
    	$this->template->render('web/user_register.html');
    }

    public function user_save()
    {
    	try 
    	{
    		$_email = $this->input->post('txt_email');
    		if (strlen($_email)<1){
	    		throw new Exception('Ingrese su email');    			
    		}
    		
    		$_password = $this->input->post('txt_password');
    		if (strlen($_password)<1){
	    		throw new Exception('Ingrese su password');    			
    		}

    		$data = array(
	    	'txt_nombre' => $this->input->post('txt_nombre'),
	    	'txt_apellidos' => $this->input->post('txt_apellidos'),
	    	'txt_email' =>$_email,
	    	'txt_email_apoderado' => $this->input->post('txt_email_apoderado'),
	    	'txt_password' => $_password
	    	);
	
	    	if ($this->user_model->check_email($this->input->post('txt_email'))){
	    		throw new Exception('Email existe');
	    	}
	    	
			if (!$this->user_model->save($data)){
	    		throw new Exception('Error al grabar lod datos');
			}
			
			$id = (int)$this->db->insert_id();
			if ($id<1){
	    		throw new Exception('ID no se genero automaticamente');				
			}
				
			
	    	$data2 = array(
	    	'fk_accion' => 3,
	    	'fk_usuario' => $id,
	    	'date_registro' =>date("Y-m-d H:i:s"),
	    	'id_usuario_accion_detalle' => $id
	    	);
			if (!$this->user_model->saveUsuarioAccion($data2))
			{
	    		throw new Exception('Error al grabar saveUsuarioAccion');
			}
						
			if (!session_id()){session_start();}
			$data_user_session['id']=$id;
			$data_user_session['name']=$data['txt_nombre'];
			$data_user_session['email']=$data['txt_email'];
			$data_user_session['logged']=true;
			$_SESSION['user_data'] = $data_user_session;
			throw new Exception('OK');
    	}
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
//    		$this->template->set('message', $e->getMessage());
//    		$this->template->render('web/user_thanks.html');
    	}
    }
    
    public function user_login()
    {
		$this->template->setLayout('layout_popup.html');
    	$this->template->render('web/user_login.html');
    }

    public function user_login_proc()
    {
    	try
    	{
	    	$txt_email = $this->input->post('txt_email');
	        $txt_password = $this->input->post('txt_password');
	        
//	        echo $txt_password;
//	        die();
	        
			if (!$this->user_model->existUser($txt_email, $txt_password)){
				throw new Exception('No existe el usuario');
			}
			
			$data = $this->user_model->getUser($txt_email, $txt_password);
			if (!session_id()){session_start();}
			$data_user_session['id']=$data->pk_usuario;;
			$data_user_session['name']=$data->txt_nombre;
			$data_user_session['email']=$data->txt_email;
			$data_user_session['logged']=true;
			$_SESSION['user_data'] = $data_user_session;
			//redirect('/');
			throw new Exception('OK');
    	}
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
			//$this->template->setLayout('layout_popup.html');
//    		$this->template->set('message', $e->getMessage());
//			$this->template->render('web/user_login_proc.html');
    	}
    }

    public function user_invite()
    {
		$this->template->setLayout('layout_popup.html');
    	$this->template->render('web/user_invite.html');
    }  
    
    public function user_invite_proc()
    {
    	try
    	{
    		
//    		print_r($_SESSION);
    		
	    	$pk_usuario = $this->__getSessionId();
			if ($pk_usuario<1){
				throw new Exception('Debe loguearse');
			}

		//	echo $this->__getSessionIdJuego();
			
			$pk_juego = $this->__getSessionIdJuego();
			
			if ($pk_juego<1){
				throw new Exception('invalido id juego');
			}

			$_email = $this->input->post('txt_email');
			//echo $_email;
			
			if (strlen($_email)<1){
				throw new Exception('invalido txt_email');				
			}
			
			$fk_invitado = $this->user_model->getPkByEmail($_email);
			if ($fk_invitado>0)
			{
				$pk_invitado = $this->user_model->existRowInvitado($pk_usuario, $pk_juego, $fk_invitado);
				if ($pk_invitado>0){
					$_state_invitado = $this->user_model->getStateInvitado($pk_invitado);
					if ($_state_invitado==1){
						$data = array(
						'int_veces'=>$this->user_model->getVecesInvitado($pk_invitado)+1,
						'date_mod'=>date("Y-m-d H:i:s")
						);
						$this->user_model->updateStateInvitado($pk_invitado, $data);
						throw new Exception('OK interno UPDATE');
					}
					else 
					{
						throw new Exception('OK interno Ya fue mandada esta invitacion al correo: '. $_email);						
					}
				}
				else 
				{
					$data = array(
						'fk_juego'=>$pk_juego,
						'fk_usuario'=>$pk_usuario,
						'fk_invitado'=>$fk_invitado,
						'int_estado'=>0,
						'int_veces'=>0,
						'date_registro'=>date("Y-m-d H:i:s")
						);
			
					if (!$this->user_model->saveInvitado($data)){
						throw new Exception('Error al grabar el registro saveInvitado');
					}
					throw new Exception('OK interno');
				}
			}
			else 
			{
				$data = array(
					'fk_juego'=>$pk_juego,
					'fk_usuario'=>$pk_usuario,
					'txt_email'=>$_email,
					'int_veces'=>0,
					'date_registro'=>date("Y-m-d H:i:s"),
					'int_tipo'=>1
					);
		
				if (!$this->user_model->saveInvitadoExterno($data)){
					throw new Exception('Error al grabar el registro saveInvitadoExterno');
				}
				throw new Exception('OK externo');
			}
    	}		
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
//			$this->template->set('message', $e->getMessage());
//			$this->template->render('web/user_invite_proc.html');
    	}
    }  

    public function user_logout()
    {
    	session_destroy();
   		unset($_SESSION);
		redirect('/');
    }    
    
    public function user_play()
    {
		$this->template->render('web/user_play_start.html');
    }    

    public function user_play_start()
    {
    	try 
    	{
    		
    		//echo $this->get();
    		
    		$pk_juego = $this->input->post('pk_juego');

    
    		
	    	$pk_usuario = $this->__getSessionId();
			if ($pk_usuario<1){
	    		throw new Exception('Debe loguearse');				
			}
			
			$pk_juego = (int)$pk_juego;
			if ($pk_juego<1){
	    		throw new Exception('Codigo jugada invalido');
			}
			
			
//				$tot_monedas = $this->user_model->getTotMonedas($pk_usuario, 'oro');
//				echo var_dump($tot_monedas);
//				echo("<br>");
//				if ($tot_monedas<1){
//					echo "ronald";
//		    		throw new Exception('Debe tener monedas para jugar');
//				}
//						
//				
//				die();	
			
//			var_dump($this->user_model->isNew($pk_usuario));
			
			
			// es Nuevo
//			if (!$this->user_model->isNew($pk_usuario))
//			{
				$tot_monedas = $this->user_model->getTotMonedas($pk_usuario, 'oro');
				if ($tot_monedas<1){
		    		throw new Exception('Debe tener monedas para jugar');
				}
//			}
//			else 
//			{
//				
//			}
			
			
			
			$date_fe = date("Y-m-d H:i:s");			
	
			$data = array(
	    	'fk_juego' => $pk_juego,
	    	'fk_usuario' => $pk_usuario,
	    	'fecha_registro' => $date_fe,
	    	'int_estado' => 0
	    	);

			if (!$this->user_model->saveUsuarioJuego($data)){
	    		throw new Exception('Error grabar: saveUsuarioJuego');
			}
	
			$id_usuario_juego = $this->db->insert_id();
			if ($id_usuario_juego<1){
	    		throw new Exception('Error generar: id $id_usuario_juego');				
			}

	    	$data = array(
	    	'fk_accion' => 2,
	    	'fk_usuario' => $pk_usuario,
	    	'date_registro' =>$date_fe,
	    	'id_usuario_accion_detalle' => $id_usuario_juego
	    	);
			if (!$this->user_model->saveUsuarioAccion($data))
			{
	    		throw new Exception('Error al grabar saveUsuarioAccion');
			}
			
			if (!session_id()){session_start();}
			$data_play['id_juego']=$pk_juego;
			$data_play['id_usuario_juego']=$id_usuario_juego;
			$_SESSION['data_play'] = $data_play;
    		//throw new Exception('O2K');
    		
    	}
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
    	//	echo("aa");
//			$this->template->set('message', $e->getMessage());
//			$this->template->set('id_juego', $pk_juego);

			//$this->template->setLayout('layout_popup.html');
			
	//		$this->template->render('web/user_play_end.html');
    	}
    }

    public function user_play_end()
    {
    	try 
    	{
	    	$id_usuario_juego = $this->__getSessionIdUsuarioJuego();
			if ($id_usuario_juego<1){
	    		throw new Exception('ID USER juego INVALID');
			}
	    	$int_estado = rand(1, 2);
			$date_fe = date("Y-m-d H:i:s");
			$data = array('int_estado'=>$int_estado, 'fecha_mod'=>$date_fe);
			$this->user_model->updateStateUsuarioJuego($id_usuario_juego, $data);
			if ($int_estado==1){
	    		throw new Exception('GANO');
			}else {
	    		throw new Exception('PERDIO');
			}
    	}
    	catch (Exception $e)
    	{
    		//	$this->template->setLayout('layout_popup2.html');
    		$this->template->set('message', $e->getMessage());
    		//$this->template->set('winner', $e->getMessage());

    		$this->template->render('web/user_play_end.html');
    	}
    }

    public function user_send_packing()
    {
		$this->template->setLayout('layout_popup.html');
		$this->template->render('web/user_packing.html');
    }

    public function user_send_packing_proc()
    {
    	try 
    	{
	    	$code = $this->input->post('txt_numero_codigo');

    		$pk_usuario = $this->__getSessionId();
			if ($pk_usuario<1){
	    		throw new Exception('usuario invalido');
			}
	    	
	    	if (!$this->user_model->validar_empaque($code)){
	    		throw new Exception('empaque invalido');
	    	}
	
	    	$int_estado = $this->user_model->getStateEmpaqueByCode($code);
	    	$pk_empaque = $this->user_model->getIdEmpaqueByCode($code);;

	    	if ($pk_empaque<1){
	    		throw new Exception('id empaque invalido');
	    	}
	    	
			if ($int_estado==1)
			{
	    		throw new Exception('empaque utilizado');
			}
			
			$date_fe= date("Y-m-d H:i:s");
			$data = array('int_estado'=>'1', 'fecha_mod'=>$date_fe);
			$this->user_model->updateStateEmpaque($code, $data);
	    	$data = array(
	    	'fk_accion' => 1,
	    	'fk_usuario' => $pk_usuario,
	    	'date_registro' =>$date_fe,
	    	'id_usuario_accion_detalle' => $pk_empaque
	    	);
			$this->user_model->saveUsuarioAccion($data);
    		throw new Exception('OK');			
    	}
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
//    		$this->template->set('message', $e->getMessage());
//			$this->template->setLayout('layout_popup.html');
//    		$this->template->render('web/user_packing_proc.html');
    	}		
    }    
    
    public function user_invitaciones()
    {
    	$pk_usuario = $this->__getSessionId();
		$this->template->set('data', $this->user_model->getInvitados($pk_usuario));
		$this->template->render('web/user_invitaciones_help.html');
    }
    
	public function user_aliados()
	{
    	$pk_usuario = $this->__getSessionId();
		$this->template->set('data', $this->user_model->getInvitados($pk_usuario));
		$this->template->render('web/user_aliados.html');
	}

    public function invitado_play_start()
    {
    	try
    	{
    		
	    	$pk_juego = $this->input->post('pk_juego');
	        $pk_usuario = $this->input->post('pk_usuario');

	        
			$pk_juego = (int)$pk_juego;
			if ($pk_juego<1){
				throw new Exception('Codigo juego invalido');
			}

    		$pk_usuario = (int)$pk_usuario;
			if ($pk_usuario<1){
				throw new Exception('Codigo usuario invalido');
			}			

	    	$pk_invitado = $this->__getSessionId();
			if ($pk_invitado<1){
				throw new Exception('Codigo invitado invalido');
			}
			
			$tot_monedas = $this->user_model->getTotMonedas($pk_invitado, 'oro');
			if ($tot_monedas<1){
	    		throw new Exception('Debe tener monedas');
			}			
			
			$date_fe = date("Y-m-d H:i:s");			
	
			//			echo $pk_usuario;
			//			echo("<br>");
			//			echo $pk_juego;
			//			echo("<br>");
			//			echo $pk_invitado;
			//			die();
						
			// ACEPTAR LA INVITACION
			$pk_invitado = $this->user_model->existRowInvitado($pk_usuario, $pk_juego, $pk_invitado);
			if ($pk_invitado<1){
	    		throw new Exception('No se puede actualizar e estado de pendiente a Aceptado');
			}
			$data = array(
			'int_estado'=>1,
			'date_mod'=>date("Y-m-d H:i:s")
			);
			$this->user_model->updateStateInvitado($pk_invitado, $data);

					
			$data = array(
	    	'fk_usuario' => $pk_invitado,
			'fk_invitado' => $pk_usuario,
	    	'fk_juego' => $pk_juego,
	    	'fecha_registro' => $date_fe,
	    	'int_estado' => 0
	    	);

			if (!$this->user_model->saveInvitadoJuego($data)){
				throw new Exception('Error al grabar el registro saveInvitadoJuego');				
			}
			
			$id_invitado_juego = $this->db->insert_id();
			if ($id_invitado_juego<1){
	    		throw new Exception('id_invitado_juego no se genero automaticamente');				
			}
						
	    	$data = array(
	    	'fk_accion' => 2,
	    	'fk_usuario' => $pk_invitado,
	    	'date_registro' =>$date_fe,
	    	'id_usuario_accion_detalle' => $id_invitado_juego
	    	);

			if (!$this->user_model->saveUsuarioAccion($data)){
	    		throw new Exception('Error al grabar el registro saveUsuarioAccion');				
			}
			
			if (!session_id()){session_start();}
			$data_play['pk_juego']=$pk_juego;
			$data_play['pk_invitado']=$pk_invitado;
			$data_play['pk_invitado_juego']=$id_invitado_juego;
			$_SESSION['data_play'] = $data_play;
			throw new Exception('OK');
    	}
    	catch (Exception $e)
    	{
    		echo $e->getMessage();
			//$this->template->set('message', $e->getMessage());
			//$this->template->set('id_juego', $pk_juego);
			//$this->template->render('web/invitado_play_end.html');
    	}
    }
        
    public function invitado_play_end()
    {
    	try
    	{
	    	$pk_invitado_juego = $this->__getSessionIdInvitadoJuego();
			if ($pk_invitado_juego<1){
	    		throw new Exception('invalid: $pk_invitado_juego');				
			}
			
	    	$pk_invitado = $this->__getSessionIdInvitado();
			if ($pk_invitado<1){
	    		throw new Exception('invalid: $pk_invitado');				
			}
						
			$int_estado = rand(1, 2);
			$date_fe = date("Y-m-d H:i:s");
			$data = array('int_estado'=>$int_estado, 'fecha_mod'=>$date_fe);
			$this->user_model->updateStateInvitadoJuego($pk_invitado_juego, $data);
				

			$data = array(
			'int_estado'=>0,
			'date_mod'=>date("Y-m-d H:i:s")
			);
			$this->user_model->updateStateInvitado($pk_invitado, $data);

						
			if ($int_estado==1){
	    		throw new Exception('Gano');				
			}else {
	    		throw new Exception('Perdio');				
			}
    	}
    	catch (Exception $e)
    	{
    	//	$this->template->setLayout('layout_popup.html');
			$this->template->set('message', $e->getMessage());
			$this->template->render('web/invitado_play_end.html');
    	}
    }
	
    public function getUsers()
	{		
	
		$this->output->enable_profiler(FALSE);

		$pk_usuario = $this->__getSessionId();
		//echo $pk_usuario;
		
		//$pk_usuario=1;
		
		if ($pk_usuario<1){
			die('Codigo usuario invalido');
		}

		$email = $this->user_model->getEmailByPk($pk_usuario);
		$emails = $this->user_model->getEmails($email);
		
	//	print_r($emails);
		
		foreach ($emails as $row){
			$key=0;
			$txt_email = $row['txt_email'];
			echo "$txt_email\n";
		}
	}


    
    public function user_trofeos()
    {
    	$this->template->render('web/user_trofeos.html');
    }
    
    public function user_notificaciones()
    {
    	$this->template->render('web/user_notificaciones.html');
    }
    
    public function user_login_proc2()
    {
    	echo "x";
    	die();
    }
    
    public function d()
    {
    	$this->db->query("delete from tbl_usuario_accion");
    	$this->db->query("delete from tbl_usuario_juego");
    	$this->db->query("delete from tbl_invitado");
    	$this->db->query("delete from tbl_invitado_externo");
    	$this->db->query("delete from tbl_invitado_juego");
    	$this->db->query("delete from tbl_usuario");
    	echo "OK";
    }    
}
?>