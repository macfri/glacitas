<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"
xmlns:fb="http://www.facebook.com/2008/fbml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>:::... Glacitas ...:::</title>
<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>web/styles/maq.css" />
<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>web/styles/web.css" />
<?php if (!empty($GLOBALS['TEMPLATE']['css'])) echo $GLOBALS['TEMPLATE']['css'] ; ?>
</head>
<body>
<div id="maq_contenedor">
  <div id="maq_cabecera">
      <p></p>
  </div>
  <div id="maq_principal">
    <?php if($GLOBALS['TEMPLATE']['interna']=='0'): ?>
    	<div id="maq_home">
	    <?php if (!empty($GLOBALS['TEMPLATE']['contenido'])) echo $GLOBALS['TEMPLATE']['contenido']; ?>
        </div>
    <?php endif; ?>

    <?php if($GLOBALS['TEMPLATE']['interna']=='1'): ?>
	<div id="maq_interna">            
            <div id="web_interna">
            	<?php if (!empty($GLOBALS['TEMPLATE']['contenido'])) echo $GLOBALS['TEMPLATE']['contenido']; ?>
            </div>
       </div>
    <?php endif; ?>

  </div>
    <?php if($GLOBALS['TEMPLATE']['interna']=='1'): ?>
      <div id="pie">
	  <p></p>
      </div>
    <?php endif; ?>
</div>
<script type="text/javascript" src="<?php echo site_url()?>web/js/jquery.js"></script>
<script type="text/javascript" src="<?php echo site_url()?>web/js/jquery.blockUI.js"></script>
<script type="text/javascript" src="<?php echo site_url()?>web/js/jquery.form.js"></script>
<script type="text/javascript" src="<?php echo site_url()?>web/js/bloqueo.js"></script>
<script type="text/javascript" src="<?php echo site_url()?>web/js/validar.js"></script>
<?php if (!empty($GLOBALS['TEMPLATE']['scripts']))echo $GLOBALS['TEMPLATE']['scripts']; ?>
</body>
</html>
