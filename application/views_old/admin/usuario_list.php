<div id="web_dinamico">
   <?php 
        $atributos = array('name' => 'frm_list_usuarios', 'id' => 'frm_list_usuarios');
        echo form_open('admin/usuario/index',$atributos);        
   ?>   
        <div id="web_moduloInterno">
            <div id="web_moduloCabecera">
                <h3>Usuarios</h3>
                <h4>LISTADO</h4>
            </div>
            <div id="web_moduloContenido">
                <div class="web_moduloBotones">
                    <a href="<?php echo site_url("admin/usuario/load");?>" onclick="this.href" class="web_boton1 ">AGREGAR</a>
                    <a href="<?php echo site_url("admin/home");?>" class="web_boton1 id_cancelar">CANCELAR</a>
                    <a href="#" onclick="javascript:caja();" class="web_boton1 id_eliminar">ELIMINAR SELECCIONADOS</a>
                </div>
                <div id="web_moduloFiltros">
                    <label class="label1">Filtrar por: </label>
                    <input type="text" id="txt_buscar" name="txt_buscar" class="inputText1" />
                    <a href="javascript:enviar();" onclick="this.href" class="web_boton1">BUSCAR</a>
                    <a href="<?php echo site_url("admin/usuario");?>" onclick="this.href" class="web_boton1">TODOS</a>
                </div>
                <div id="web_moduloListadoRegistros">
                    <table cellspacing="0" summary="registros" class="web_tableRegistros" id="id_tabla_listado">
                        <tr class="web_trCabecera">
                            <th class="web_thCheck1"><input type="checkbox" name="id_check_todos" id="id_check_todos" /></th>
                            <th class="web_thColumna1">Usuario</th>
                            <th class="web_thColumna1">E-mail</th>
                            <th class="web_thColumna5">Acciones</th>
                        </tr>
                        <?php
                          
                        foreach($usuario as $usuarios): ?>
                        <tr class="web_trFilaRegistros1">
                            <td class="web_tdCheck1">
                                <input type="checkbox" name="grupo_check[]" id="check_<?php echo $usuarios->pk_usuario?>" value="<?php echo $usuarios->pk_usuario?>" />
                            </td>
                            <td><?php echo $usuarios->txt_login;?></td>
                            <td><?php echo $usuarios->txt_correo;?></td>
                            <td>
                                <a href="<?php echo site_url("admin/usuario/load/id/$usuarios->pk_usuario");?>">
                                    <img src="<?php echo site_url();?>images/icono_editar.gif" width="24" border="0" height="24" alt="Editar" title="Editar" />
                                </a>
                                <a href="<?php echo site_url("admin/usuario/delete/id/$usuarios->pk_usuario");?>">
                                    <img src="<?php echo site_url();?>images/icono_eliminar.gif" width="24" height="24" border="0" alt="Eliminar" title="Eliminar" />
                                </a>
                            </td>
                        </tr>
                        <?php endforeach;?>

                    </table>
                </div>
                <div id="web_moduloPaginacion">                   
                    <p><?php if(isset($pagination)) echo $pagination; ?></p>
                </div>
                <div class="web_moduloBotones">
                    <a href="<?php echo site_url("admin/usuario/load");?>" onclick="this.href" class="web_boton1 ">AGREGAR</a>
                    <a href="<?php echo site_url("admin/home");?>" class="web_boton1 id_cancelar">CANCELAR</a>
                    <a href="#" onclick="javascript:caja();" class="web_boton1 id_eliminar">ELIMINAR SELECCIONADOS</a>
                </div>
                <br class="clearIzq" />
            </div>
      </div>
    <!--</form>-->
    <? echo form_close();?>
</div>
 
<script type="text/javascript" >
        function enviar(){
            document.forms["frm_list_usuarios"].submit();
        }
        function caja(){
            document.forms["frm_list_usuarios"].action="usuario/delete_seleccionado";
            document.forms["frm_list_usuarios"].submit();
        }
    </script>