<?php
ob_start();
?>
<div id="usuario_nuevo">
    <div id="formulario">
    <?php
        $atributos = array('name' => 'login', 'id' => 'login');
        echo form_open('login/proc_login', $atributos);
    ?>
        <h3><b>¿Ya está registrado?</b></h3>
        <p >
                <label for="email">Correo electrónico</label>
                <span><input type="text" size="35" title="coloque su correo" class="caja_texto requerido " value="" name="txt_email" id="txt_email" ></span>
        </p>
        <p >
                <label for="passwd">Contraseña</label>
                <span><input type="password" size="35" title="coloque su contraseña" class="caja_texto requerido" value="" name="txt_password" id="txt_password"></span>
        </p>        
        <p >
            <a href="#enviarLogin" class="web_boton1 validar btn_envia">ENVIAR</a>
            <?php //echo form_submit('enviar', 'Enviar'); ?>
            <a class="web_boton1 btn_recupera" href="http://www.utilesperu.com/password.php">¿Ha olvidado su contraseña?</a>
        </p>

     <? echo form_close();?>
    </div>
</div>

<?php
$GLOBALS['TEMPLATE']['contenido'] = ob_get_clean();
ob_start();
$GLOBALS['TEMPLATE']['scripts'] = ob_get_clean();
$GLOBALS['TEMPLATE']['interna'] = 1;
include 'base.php';
?>