<?php
ob_start();
?>
<?php echo validation_errors('<div class="error">','</div>')?>
<?php echo form_open_multipart('login/validate');?>
<input type="hidden" name="pk_usuario" value="<?php echo $usuario[0]->pk_usuario; ?>" />
<input type="hidden" value="<?php echo site_url()."login/confirmar";?>" id="id_listado_url" />
<div id="usuario_nuevo">
    <div id="formulario">       
        <h3><b>Registro de usuario</b></h3>
        <p>
            <label >Nombre</label>
            <span><input type="text" size="35" title="Ingrese su nombre" class="caja_texto requerido" value="" name="txt_nombre"></span>
        </p>
        <p>
            <label >Apellidos</label>
            <span><input type="text" size="35" title="Ingrese su apellidos" class="caja_texto requerido" value="" name="txt_apellidos"></span>
        </p>
        <p>
            <label>email</label>
            <span><input type="text" size="35" title="Ingrese su email" class="caja_texto requerido" value="" name="txt_email" id="txt_email"></span>
        </p>
        <p>
            <label>Fecha Nacimiento</label>
            <span><input id="date_fecha_nacimiento"  name="date_fecha_nacimiento" title="Fecha de nacimiento" class="date-pick text_form_fecha requerido" value=""  type="text" size="15"/></span>
        </p>
        <p>
            <label>Usuario</label>
            <span><input type="text" size="35" title="Ingrese usuario" class="caja_texto requerido" value="" name="txt_nickname"></span>
        </p>
        <p>
            <label>Contrase&ntilde;a</label>
            <span><input type="password" size="35" title="Ingrese su contraseña" class="caja_texto requerido" value="" name="txt_password"></span>
        </p>
        <p>
            <label>Confirmar contrase&ntilde;a</label>
            <span><input type="password" size="35" title="Ingrese su email" class="caja_texto requerido" value="" name="txt_re_password"></span>
        </p>
        <p>
            <label>Nombre apoderado</label>
            <span><input type="text" size="35" title="Ingrese el nombre del apoderado" class="caja_texto requerido" value="" name="txt_apoderado"></span>
        </p>
        <p>
            <label>email apoderado</label>
            <span><input type="text" size="35" title="Ingrese el correo del apoderado" class="caja_texto requerido" value="" name="txt_email_apoderado" id="txt_email_apoderado"></span>
        </p>
        <p >
<!--            <a href="#enviar" class="web_boton1 validar btn_envia">ENVIAR</a>-->
            <?php echo form_submit('enviar', 'Enviar'); ?>
        </p>
    </div>
</div>
<?php echo form_close(); ?>
<?php
$GLOBALS['TEMPLATE']['contenido'] = ob_get_clean();
ob_start();
?>
<script type="text/javascript" src="<?php echo site_url()?>web/js/calendar/ui.datepicker.js"></script>
<script type="text/javascript" src="<?php echo site_url()?>web/js/calendar/calendarfrm.js"></script>

<link rel="stylesheet" type="text/css" href="<?php echo site_url()?>web/styles/calendar/ui.datepicker.css" />
<?
$GLOBALS['TEMPLATE']['scripts'] = ob_get_clean();
$GLOBALS['TEMPLATE']['interna'] = 1;
include 'base.php';
?>