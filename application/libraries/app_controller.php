<?php
class app_controller extends Controller {

    function app_controller()
    {
        parent::Controller();   
		$this->load->model('user_model');
		$this->load->library('Template');
		$this->template->set('tot_monedas_oro', $this->user_model->getTotMonedas($this->__getSessionId(), 'oro'));
		$this->template->set('tot_monedas_plata', $this->user_model->getTotMonedas($this->__getSessionId(), 'plata'));
	}
	
    public function getSession()
    {
		if (!session_id()){session_start();}
		print_r($_SESSION);
    }
    
    public function d()
    {
    	$this->db->query("delete from tbl_usuario_accion");
    	$this->db->query("delete from tbl_usuario_juego");
    	$this->db->query("delete from tbl_invitado");
    	$this->db->query("delete from tbl_invitado_externo");
    	$this->db->query("delete from tbl_invitado_juego");
    	//$this->db->query("delete from tbl_usuario");
    	echo "OK";
    }
    
	protected  function __getSessionId()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['user_data']['id'])){
			$session_id = (int)$_SESSION['user_data']['id'];
		}
		return $session_id;
    }

    protected function __getSessionIdJuego()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['id_juego'])){
			$session_id = (int)$_SESSION['data_play']['id_juego'];
		}
		return $session_id;
    }
    
    protected function __getSessionIdUsuarioJuego()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['id_usuario_juego'])){
			$session_id = (int)$_SESSION['data_play']['id_usuario_juego'];
		}
		return $session_id;
    }    
    
    protected function __getSessionIdInvitadoJuego()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['pk_invitado_juego'])){
			$session_id = (int)$_SESSION['data_play']['pk_invitado_juego'];
		}
		return $session_id;
    }   
    
    protected function __getSessionIdInvitado()
    {
		if (!session_id()){session_start();}
		$session_id=0;
		if (isset($_SESSION['data_play']['pk_invitado'])){
			$session_id = (int)$_SESSION['data_play']['pk_invitado'];
		}
		return $session_id;
    }
    
  	protected function __validateEmail($email)
  	{
  		$out = false;  		
		if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email))
		{
			$out=true;		
		}
		return $out;
  	}
}
?>