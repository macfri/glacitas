<?php
/*****
* Generator Class MC v.1.55
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
* Descripcion: Este Modelo controla la tabla de la base de datos con su mismo nombre
******/

/***
* @EXTIENDE EL MODELO
* Descripcion: se utilizara para nuevas funciones
* Creador: Marco Cristobal D.
* Fecha: 20/01/2011
****/	

class tbl_notificaciones_model extends Model{
	function __construct(){
        parent::Model();
        $this->load->model("base/Base_tbl_notificaciones_model","obj_notificaciones");
    }
	
} //FIN DEL MODELO EXTENDIDO
?>