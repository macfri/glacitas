<?php
/*****
* Generator Class MC v.1.55
* Phantasia tribal DDB.
* Proyecto
* V. 1.0
* Iniciado: 20/01/2011
* Descripcion: Este Modelo controla la tabla de la base de datos con su mismo nombre
******/

/***
* @EXTIENDE EL MODELO
* Descripcion: se utilizara para nuevas funciones
* Creador: Marco Cristobal D.
* Fecha: 20/01/2011
****/	

class tbl_usuario_model extends Model{
    function __construct(){
        parent::Model();
        $this->load->model("base/Base_tbl_usuario_model","obj_usuario");
    }
    function verificar_email($txt_email){
        $this->db->select('txt_email');
        $this->db->where('txt_email',$txt_email);
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        if ($dato){
            return true;
        }
        return false;
    }

    function verificar_password($txt_password){        
        $this->db->select('txt_password');
        $this->db->where('txt_password',$txt_password);
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        if ($dato){
            return true;
        }
        return false;
        
    }

    function verificar_cuenta($mail, $pass){        
        $this->db->select('txt_password,txt_email,int_activo');
        $this->db->where('txt_email',$mail);
        $this->db->where('txt_password',$pass);
        $this->db->where('int_activo','1');
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        if ($dato){
            return true;
        }
        return false;
    }

    function recuperar_usuario($mail_h = null, $mail_p = null, $usuario = null ){
        $this->db->select('pk_usuario, txt_nickname, txt_password');
        $this->db->where('txt_email', $mail_h);
        $this->db->where('txt_email_apoderado', $mail_p);
        $this->db->where('txt_nickname', $usuario);
        $this->db->limit(1);
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        
        return $dato;
    }

    function activar_usuario($pk_usuario = null, $data = null){
        $this->db->where('pk_usuario', $pk_usuario);
        $this->db->update('tbl_usuario', $data);
    }

    function datos_usuario($pk_usuario = null){
        $this->db->select('pk_usuario, txt_nickname');
        $this->db->where('pk_usuario', $pk_usuario);
        $this->db->limit(1);
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        return $dato;
    }

    function existe_mail($mail = null){       
        $this->db->select('txt_email');
        $this->db->where('txt_email', $mail);        
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        if ( count($dato) > 0 ){
            return false;
        }else{            
            return true;
        }
    }

    function existe_usuario($usuario = null){
        $this->db->select('txt_nickname');
        $this->db->where('txt_nickname', $usuario);
        $this->db->from('tbl_usuario');
        $query = $this->db->get();
        $dato = $query->result();
        if ( count($dato) > 0 ){
            return false;
        }else{
            return true;
        }
    }
	
} //FIN DEL MODELO EXTENDIDO
?>