<?php
class user_model extends Model
{
    function __construct()
    {
        parent::Model();
    }
    
    function check_exist($e, $p)
    {
        $this->db->where('txt_email',$e);
        $this->db->where('txt_password',$p);
        $query = $this->db->get('tbl_usuario');
        $tot = $query->num_rows>0?true:false;
        return $tot;
    }
    
    function check_email($e)
    {
        $this->db->where('txt_email',$e);
        $query = $this->db->get('tbl_usuario');
        $tot = $query->num_rows>0?true:false;
        return $tot;
    }    
    
	function getUser($txt_email, $txt_password)
	{
		$sql = "select * FROM tbl_usuario where 
		txt_email='$txt_email' and txt_password='$txt_password' limit 1";
		$data = $this->db->query($sql);
		if ($data->num_rows>0){
			$e = $data->result();
		}
		if (isset($e[0]) && is_object($e[0])){
			return $e[0];
		}else {
			return array();
		}
	}
	
	function existUser($txt_email, $txt_password)
	{
        $this->db->where('txt_email',$txt_email);
        $this->db->where('txt_password',$txt_password);
        $query = $this->db->get('tbl_usuario');
        return $query->num_rows>0?true:false;		
	}
    
	function save($data)
	{
		return $this->db->insert('tbl_usuario', $data);
	}
	
	function getIdByEmail($txt_email)
	{
		$pk_invitado = 0;
    	$sql = "select pk_usuario as ID  FROM tbl_usuario where txt_email='$txt_email' limit 1";
    	$data = $this->db->query($sql);
    	if ($data->num_rows>0){
			$e = $data->result();
    	}
		if (isset($e[0]) && is_object($e[0]) && property_exists($e[0],'ID')){
		    $pk_invitado=(int)$e[0]->ID;			
		}
    	return $pk_invitado;
	}
	
	function saveUsuarioJuego($data)
	{
		return $this->db->insert('tbl_usuario_juego', $data);
	}
	
	function saveInvitadoJuego($data)
	{
		return $this->db->insert('tbl_invitado_juego', $data);
	}

	function saveUsuarioAccion($data)
	{
		return $this->db->insert('tbl_usuario_accion', $data);
	}	
	
	function getIdEmpaqueByCode($code)
	{
    	$sql = "select 	pk_empaque
    			from 
    					tbl_empaque 
    			where 
    					txt_numero_codigo='$code' limit 1";
    	$rs = $this->db->query($sql);
    	
//    	print_r($rs);
    	
    	
    	$pk_empaque = 0;
    	if ($rs->num_rows>0){
    		$data = $rs->result_array();
    		$pk_empaque = isset($data[0]['pk_empaque'])?(int)$data[0]['pk_empaque']:0;
		}
		return $pk_empaque;
	}	
	
	function getStateEmpaqueByCode($code)
	{
    	$sql = "select 	int_estado
    			from 
    					tbl_empaque 
    			where 
    					txt_numero_codigo='$code' limit 1";
    	$rs = $this->db->query($sql);
    	$int_estado = 0;
    	if ($rs->num_rows>0){
    		$data = $rs->result_array();
    		$int_estado = isset($data[0]['int_estado'])?(int)$data[0]['int_estado']:0;
		}
		return $int_estado;
	}	

	function getPkByEmail($txt_email)
	{
    	$pk_invitado=0;
    	$sql = "select pk_usuario  FROM tbl_usuario where txt_email='$txt_email' limit 1";
    	$data = $this->db->query($sql);
    	if ($data->num_rows>0){
			$e = $data->result();
			if (isset($e[0]) && is_object($e[0]) && property_exists($e[0],'pk_usuario')){
			    $pk_invitado=(int)$e[0]->pk_usuario;
			}
		}		
		return $pk_invitado;
	}
	
	function updateStateUsuarioJuego($id, $state)
	{
		$this->db->where("pk_usuario_juego",$id);
		$this->db->update('tbl_usuario_juego', $state);
	}
	
	function saveInvitado($data)
	{
		return $this->db->insert('tbl_invitado', $data);		
	}
	
	function getInvitados($pk_usuario)
	{
		$this->db->select('
		tbl_usuario.pk_usuario,
		tbl_usuario.txt_nombre as nombre_usuario,
		tbl_juego.txt_nombre as nombre_juego,
		tbl_juego.pk_juego,
		tbl_invitado.int_estado,
		tbl_invitado.fk_invitado
		');
        $this->db->where('fk_invitado',$pk_usuario);
        $this->db->from('tbl_invitado');
        $this->db->join('tbl_usuario','tbl_usuario.pk_usuario=tbl_invitado.fk_usuario');
        $this->db->join('tbl_juego','tbl_juego.pk_juego=tbl_invitado.fk_juego');
        $query = $this->db->get();
		return $query->result();
	}
	
	function getAliados($pk_usuario)
	{
		$this->db->select('
		tbl_usuario.pk_usuario,
		tbl_usuario.txt_nombre as nombre_usuario,
		tbl_juego.txt_nombre as nombre_juego,
		tbl_juego.pk_juego,
		tbl_invitado.int_estado,
		tbl_invitado.fk_invitado
		');
        $this->db->where('fk_usuario',$pk_usuario);
        $this->db->from('tbl_invitado');
        $this->db->join('tbl_usuario','tbl_usuario.pk_usuario=tbl_invitado.fk_invitado');
        $this->db->join('tbl_juego','tbl_juego.pk_juego=tbl_invitado.fk_juego');
        $query = $this->db->get();
		return $query->result();
	}

	function updateStateInvitadoJuego($id, $state)
	{
		$this->db->where("pk_invitado_juego",$id);
		$this->db->update('tbl_invitado_juego', $state);
	}
	
	function updateStateEmpaque($txt_codigo, $data)
	{
		$this->db->where("txt_numero_codigo",$txt_codigo);
		$this->db->update('tbl_empaque', $data);
	}
	
	function getTotMonedadByUser($pk_usuario, $accion, $tipo='oro')
	{
		if ($tipo=='oro'){
			$column = 'valor_moneda_oro';
		}else {
			$column = 'valor_moneda_plata';			
		}
		$sql = "select sum(a.$column) as tot from tbl_usuario_accion  as ua, tbl_accion as a
				where
				ua.fk_accion = a.pk_acccion and
				a.pk_acccion = $accion and
				ua.fk_usuario = '$pk_usuario'
		";
		$rs = $this->db->query($sql);
		$data = $rs->result_array();
		return isset($data[0]['tot'])?$data[0]['tot']:0;
	}
	
	function getTotMonedas($pk_usuario, $oro='oro')
	{
		
//		if ($oro='oro')
//		
//		{
//			
//		}
		
		$tot_monedas_suma  = $this->getTotMonedadByUser($pk_usuario, '1', $oro);	// Registro empaque
		$tot_monedas_suma2  = $this->getTotMonedadByUser($pk_usuario, '3', $oro);	// Registro usuario


		$tot_monedas_suma = $tot_monedas_suma+$tot_monedas_suma2;
		
		$tot_monedas_resta = $this->getTotMonedadByUser($pk_usuario, '2', $oro);	// Registro juego
		
				
		//	$this->output->enable_profiler(TRUE);
		//echo $tot_monedas_suma;
//		die();
//		die();
		
		
		$tot = 0;
		if ($tot_monedas_suma>$tot_monedas_resta){
			$tot =$tot_monedas_suma-$tot_monedas_resta;
		}
		

		
		return $tot;
	}
	
	function validar_empaque($txt_numero_codigo)
	{
		if (strlen($txt_numero_codigo)<1){
			return false;
		}else {
			return true;
		}
	}
	
	function getEmails($txt_email)
	{
		$rs = $this->db->query("select txt_email from tbl_usuario where txt_email<>'$txt_email'");
		$items = $rs->result_array();
		return $items;
	}
	
	function getEmailByPk($id)
	{
    	$pk_invitado='';
    	$sql = "select txt_email  FROM tbl_usuario where pk_usuario='$id' limit 1";
    	$data = $this->db->query($sql);
    	if ($data->num_rows>0){
			$e = $data->result();
			if (isset($e[0]) && is_object($e[0]) && property_exists($e[0],'txt_email')){
			    $pk_invitado=$e[0]->txt_email;
			}
		}		
		return $pk_invitado;
	}
	
	function saveInvitadoExterno($data)
	{
		return $this->db->insert('tbl_invitado_externo', $data);
	}
	
	function existRowInvitado($fk_usuario, $fk_juego, $fk_invitado)
	{
    	$sql = "select pk_invitado as id from tbl_invitado 
    	where fk_usuario='$fk_usuario' and fk_juego='$fk_juego' and fk_invitado='$fk_invitado'";
    	$rs = $this->db->query($sql);
    	$data = $rs->result_array();
    	return isset($data[0]['id'])?(int)$data[0]['id']:0;
	}
	
	function updateStateInvitado($pk_invitado, $data)
	{
		$this->db->where("pk_invitado",$pk_invitado);
		$this->db->update('tbl_invitado', $data);
	}
	
	function getVecesInvitado($pk_invitado)
	{
		$veces = 0;
    	$sql = "select int_veces FROM tbl_invitado where pk_invitado='$pk_invitado' limit 1";
    	$data = $this->db->query($sql);
    	if ($data->num_rows>0){
			$e = $data->result_array();
			if (isset($e[0]['int_veces'])){
				$veces = $e[0]['int_veces'];
			}
    	}
    	return $veces;
	}
	
	function getStateInvitado($pk_invitado)
	{
		$estado = 1;
    	$sql = "select int_estado FROM tbl_invitado where pk_invitado='$pk_invitado' limit 1";
    	$data = $this->db->query($sql);
    	if ($data->num_rows>0){
			$e = $data->result_array();
			if (isset($e[0]['int_estado'])){
				$estado = $e[0]['int_estado'];
			}
    	}
    	return $estado;
	}	
	
	function getTrofeos()
	{
		$sql = "
select tot, (tot/5) as trofeos from
(
	select 
	(
		(
			(select (sum(int_estado)*2) as tot_puntos  from  tbl_invitado_juego  where fk_usuario=3)+
			(select sum(int_estado) as tot from tbl_usuario_juego where fk_usuario=3)
		) 
	) as tot
)
as
t
		
		";
	}
	
	
	function isNew($pk_usuario)
	{
    	$sql = "select count(*) as tot from tbl_usuario_accion where fk_usuario='$pk_usuario' limit 1";
    	$rs = $this->db->query($sql);
    	if (!$rs){
			echo $this->db->_error_message();
			exit(0);
    	}
    	$data = $rs->result_array();
    	$tot = isset($data[0]['tot'])?(int)$data[0]['tot']:0;
    	return $tot>0?false:true;
	}	
}