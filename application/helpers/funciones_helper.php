<?php
function inc_invertir_fecha($fecha,$caso)
{
	if($caso==1)
	{   //tipo 2005-04-10
		$fecha_invertida = substr($fecha,6,4) ."-". substr($fecha,3,2) ."-".substr($fecha,0,2);
	}elseif($caso==2)
	{  //tipo 10-04--2005
		$fecha_invertida = substr($fecha,8,2) ."-". substr($fecha,5,2) ."-".substr($fecha,0,4);
	}

	if($fecha_invertida=="--"){ $fecha_invertida=""; }
	return $fecha_invertida;

}

?>
