<style type="ttext/css" media="screen">
    div.error{
        background-color:#ff8f8f;
        border: 1px solid #ff1111;
        margin-bottom: 5px;
        padding: 5px;
        width: 400px;
    }
</style>
<?php echo validation_errors('<div class="error">','</div>')?>
<div id="web_dinamico">
    <?php echo form_open_multipart('admin/usuario/validate');?>
    <input type="hidden" name="pk_usuario" value="<?php echo $usuario[0]->pk_usuario; ?>">
    <table border="0">
         <tr>
            <td>Nombre</td>
            <td><input name="txt_nombre" type="text" value="<?php echo $usuario[0]->txt_nombre; ?>" /></td>
        </tr>
        <tr>
            <td>Usuario</td>
            <td><input name="txt_login" type="text" value="<?php echo $usuario[0]->txt_login; ?>" /></td>
        </tr>  
         <tr>
            <td>Password</td>
            <td><input name="txt_pass" type="password" value="<?php echo $usuario[0]->txt_pass; ?>" /></td>
        </tr>      
        <tr>
            <td valign="top">E-mail</td>
            <td><input name="txt_correo" type="text" value="<?php echo $usuario[0]->txt_correo; ?>" /></td>
        </tr>
        
        <tr>
            <td><?php echo form_submit('Enviar', 'Enviar');?></td>
            <td><a href="<?php echo site_url("admin/usuario");?>" onclick="this.href" class="web_boton1">Cancelar</a></td>
        </tr>
    </table>
    <?php echo form_close();?>
</div>